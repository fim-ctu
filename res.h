#ifndef CTU_RES_H
#define CTU_RES_H

#define CTU_MENUDLG (100)
#define CTU_INFODLG (101)

#define CTU_CTRL_LBL (200)
#define CTU_CTRL_CENT (220)
#define CTU_CTRL_LEFT (240)
#define CTU_CTRL_RIGHT (260)

#define CTU_INF_LLBL (300)
#define CTU_INF_CLBL (400)
#define CTU_INF_RLBL (500)

#define CTU_ARROW_L  (600)
#define CTU_ARROW_UL (601)
#define CTU_ARROW_U  (602)
#define CTU_ARROW_UR (603)
#define CTU_ARROW_R  (604)
#define CTU_ARROW_DR (605)
#define CTU_ARROW_D  (606)
#define CTU_ARROW_DL (607)

#define CTU_ATK_A (608)
#define CTU_ATK_B (609)
#define CTU_ATK_C (610)
#define CTU_ATK_D (611)

#endif /* CTU_RES_H */
