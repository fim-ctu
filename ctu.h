#ifndef CTU_CTU_H
#define CTU_CTU_H

#define _WIN32_WINNT 0x0500

#include <windows.h>
#include <stdint.h>

#include "res.h"
#include "menu.h"
#include "info.h"

#define CTU_MENUON    (WM_APP + 0x0)
#define CTU_MENUOFF   (WM_APP + 0x1)
#define CTU_MENUINPUT (WM_APP + 0x2)
#define CTU_SETTITLE  (WM_APP + 0x3)
#define CTU_THREADUPDATE (WM_APP + 0x4)
#define CTU_INFOON       (WM_APP + 0x5)
#define CTU_INFOOFF      (WM_APP + 0x6)
#define CTU_INFOUPDATE   (WM_APP + 0x7)
#define CTU_INPUT        (WM_APP + 0x8)

extern DWORD proc_thread_id;
extern HINSTANCE hinst;
extern DWORD mainthread;

#define ctud(...) do { \
	fprintf(stderr, __VA_ARGS__); \
	fflush(stderr); \
} while (0)

#endif /* CTU_CTU_H */
