#include "ctu.h"

#include <windows.h>
#include <shlwapi.h>
#include <stdio.h>

#include "res.h"
#include "inject.h"
#include "menu.h"

HINSTANCE hinst;

static HANDLE proc;
static HANDLE proc_thread;
DWORD proc_thread_id;

static int recording;
static int input_swapped;
static int input_replay;
static int p2_input;
static int pausemenu_active = 0;
static int p2_block;

static int16_t p1_magic_max;
static int16_t p2_magic_max;

static int infodlg_on;

/* recording history stuff */
struct input {
	int count;
	int val;
};

static struct input_history {
	struct input *ih_vals;
	struct input *ih_cur;
	int ih_pos;
	int ih_curcount;
	int ih_size;
	int ih_alloc;
} hist;

static void
recording_alloc(void)
{
	hist.ih_vals = realloc(hist.ih_vals,
	                       sizeof(hist.ih_vals[0]) * hist.ih_alloc);
}

static void
recording_start(void)
{
	void *vals_save = hist.ih_vals;
	memset(&hist, 0, sizeof(hist));
	hist.ih_alloc = 1024;
	hist.ih_vals = vals_save;
	recording_alloc();
	hist.ih_cur = NULL;
}

static void
recording_rec(int val)
{
	if (!hist.ih_cur && val == 0) {
		/* don't actually start recording until we get a real input */
		return;
	}

	if (!hist.ih_cur || hist.ih_cur->val != val) {
		if (hist.ih_size >= hist.ih_alloc) {
			hist.ih_alloc += 1024;
			recording_alloc();
		}
		hist.ih_cur = &hist.ih_vals[hist.ih_size++];
		hist.ih_cur->val = val;
		hist.ih_cur->count = 1;
	} else {
		hist.ih_cur->count++;
	}
}

static void
recording_rewind(void)
{
	if (hist.ih_cur && hist.ih_cur->val == 0) {
		/* if we ended with a neutral input, trim it from the end */
		hist.ih_size--;
	}

	if (hist.ih_size) {
		hist.ih_cur = &hist.ih_vals[0];
	} else {
		hist.ih_cur = NULL;
	}
	hist.ih_pos = 0;
}

static int
recording_play(void)
{
	if (!hist.ih_cur) {
		return 0;
	}
	if (hist.ih_curcount >= hist.ih_cur->count) {
		hist.ih_pos++;
		hist.ih_curcount = 0;
		if (hist.ih_pos >= hist.ih_size) {
			/* reset back to the beginning, but give ourselves 1 frame of neutral */
			hist.ih_pos = 0;
			hist.ih_cur = &hist.ih_vals[0];
			return 0;
		}
		hist.ih_cur = &hist.ih_vals[hist.ih_pos];
	}
	hist.ih_curcount++;
	return hist.ih_cur->val;
}

static int
translate(int val)
{
	if ((val & (CTRL_BACK | CTRL_FWD))) {
		val ^= (CTRL_BACK | CTRL_FWD);
	}
	return val;
}

static const char *ctrl_state = "CONTROLLING P1";
static void
switch_state(const char *str)
{
	ctud("\n%s", str);
	fflush(stdout);
	ctrl_state = str;
}

static void
output_ctrl(int val)
{
	static int last_val;
	static int ignore_ud = CTRL_UP;
	static int ignore_bf = CTRL_FWD;

	if (!infodlg_on) {
		return;
	}

	switch (val & (CTRL_BACK | CTRL_FWD)) {
	case CTRL_BACK: ignore_bf = CTRL_FWD;  break;
	case CTRL_FWD:  ignore_bf = CTRL_BACK; break;
	}
	switch (val & (CTRL_UP | CTRL_DOWN)) {
	case CTRL_UP:   ignore_ud = CTRL_DOWN; break;
	case CTRL_DOWN: ignore_ud = CTRL_UP;   break;
	}

	if ((val & (CTRL_BACK | CTRL_FWD)) == (CTRL_BACK | CTRL_FWD)) {
		val ^= ignore_bf;
	}
	if ((val & (CTRL_UP | CTRL_DOWN)) == (CTRL_UP | CTRL_DOWN)) {
		val ^= ignore_ud;
	}

	if (val == last_val || !val) {
		return;
	}
	last_val = val;
	if (!PostThreadMessage(mainthread, CTU_INPUT, 0, (LPARAM)val)) {
		ctud("PostThreadMessage (CTU_INPUT) %d\n", (int)GetLastError());
	}
}

DWORD mainthread;
static unsigned char p1_reverse;

struct comboinfo {
	void *addr_health;
	void *addr_meter;
	void *addr_meter_partial;
	void *addr_meter_partial_max;
	void *addr_magic;

	int start_health;
	unsigned char start_meter[2];
	int16_t start_magic;

	int combo_health;
	float combo_meter;
	int combo_magic;
};

static struct comboinfo comboinfo_p1 = {
	.addr_health = P2_HP_VADDR,
	.addr_meter = P1_SUPER_BARS_VADDR,
	.addr_meter_partial = P1_SUPER_PARTIAL_VADDR,
	.addr_meter_partial_max = P1_SUPER_PARTIAL_MAX_VADDR,
	.addr_magic = P1_MAGIC_VADDR,
};
static struct comboinfo comboinfo_p2 = {
	.addr_health = P1_HP_VADDR,
	.addr_meter = P2_SUPER_BARS_VADDR,
	.addr_meter_partial = P2_SUPER_PARTIAL_VADDR,
	.addr_meter_partial_max = P2_SUPER_PARTIAL_MAX_VADDR,
	.addr_magic = P2_MAGIC_VADDR,
};

static void
combo_update(struct comboinfo *cinfo)
{
	int cur_health;
	unsigned char cur_meter[2];
	int16_t cur_magic, start_magic;
	unsigned char max_partial_meter;

	ReadProcessMemory(proc, cinfo->addr_health, &cur_health,
	                  sizeof(cur_health), NULL);
	if (cur_health >= cinfo->start_health) {
		cinfo->combo_health = 0;
		return;
	}
	ReadProcessMemory(proc, cinfo->addr_meter, &cur_meter[0],
	                  sizeof(cur_meter[0]), NULL);
	ReadProcessMemory(proc, cinfo->addr_meter_partial, &cur_meter[1],
	                  sizeof(cur_meter[1]), NULL);
	ReadProcessMemory(proc, cinfo->addr_magic, &cur_magic,
	                  sizeof(cur_magic), NULL);
	ReadProcessMemory(proc, cinfo->addr_meter_partial_max, &max_partial_meter,
	                  sizeof(max_partial_meter), NULL);

	cinfo->combo_health = cinfo->start_health - cur_health;
	cinfo->combo_meter = (cur_meter[0] +
	                      ((float)cur_meter[1])/((float)max_partial_meter)) -
	                     (cinfo->start_meter[0] +
	                      ((float)cinfo->start_meter[1])/((float)max_partial_meter));

	start_magic = cinfo->start_magic;
	if (cur_magic < 0) {
		cur_magic *= -1;
	}
	if (start_magic < 0) {
		start_magic *= -1;
	}
	cinfo->combo_magic = cur_magic - start_magic;
}

static void
combo_reset(struct comboinfo *cinfo)
{
	ReadProcessMemory(proc, cinfo->addr_health, &cinfo->start_health,
	                  sizeof(cinfo->start_health), NULL);
	ReadProcessMemory(proc, cinfo->addr_meter, &cinfo->start_meter[0],
	                  sizeof(cinfo->start_meter[0]), NULL);
	ReadProcessMemory(proc, cinfo->addr_meter_partial, &cinfo->start_meter[1],
	                  sizeof(cinfo->start_meter[1]), NULL);
	ReadProcessMemory(proc, cinfo->addr_magic, &cinfo->start_magic,
	                  sizeof(cinfo->start_magic), NULL);
}

static void
fimwindow(void)
{
	/* the FiM window should now exist */
	if (!PostThreadMessage(mainthread, CTU_SETTITLE, 0, 0)) {
		ctud("PostThreadMessage (FIMWINDOW) %d\n", (int)GetLastError());
	}
}

static void
activate_pausemenu(void)
{
	ReadProcessMemory(proc, P1_REVERSE_VADDR, &p1_reverse, sizeof(p1_reverse), NULL);

	if (!PostThreadMessage(mainthread, CTU_MENUON, 0, 0)) {
		ctud("PostThreadMessage (MENUON) %d\n", (int)GetLastError());
	}

	pausemenu_active = 1;
}

static void
pause_ctrl(int val)
{
	static int last_val;
	if (last_val == val) {
		return;
	}
	last_val = val;
	if (!val) {
		return;
	}
	if (!PostThreadMessage(mainthread, CTU_MENUINPUT, (WPARAM)val, 0)) {
		ctud("PostThreadMessage (MENUINPUT) %d\n", (int)GetLastError());
	}
}

static int refill_health = 1;
static int refill_meter = 0;
static int refill_magic = 0;
static int deplete_magic = 0;

static int
dlgval(int index)
{
	return dlg[index].val_arr[dlg[index].cur].val;
}

static void
deactivate_infodlg(void)
{
	if (!infodlg_on) {
		return;
	}
	infodlg_on = 0;
	if (!PostThreadMessage(mainthread, CTU_INFOOFF, 0, 0)) {
		ctud("PostThreadMessage (INFOOFF) %d\n", (int)GetLastError());
	}
}
static void
activate_infodlg(void)
{
	if (infodlg_on) {
		return;
	}
	infodlg_on = 1;
	if (!PostThreadMessage(mainthread, CTU_INFOON, 0, 0)) {
		ctud("PostThreadMessage (INFOON) %d\n", (int)GetLastError());
	}
}

static void
deactivate_pausemenu(void)
{
	if (!PostThreadMessage(mainthread, CTU_MENUOFF, 0, 0)) {
		ctud("PostThreadMessage (MENUOFF) %d\n", (int)GetLastError());
	}
	pause_ctrl(0);
	pausemenu_active = 0;

	input_swapped = input_replay = recording = p2_block = 0;
	recording_rewind();

	switch (dlgval(CTU_IDX_ACTION)) {
	case CTU_DLGITEM_Control:
		ctud("**INPUT SWAPPED\n");
		input_swapped = 1;
		break;
	case CTU_DLGITEM_Record:
		ctud("**RECORDING P2\n");
		input_swapped = 1;
		recording = 1;
		recording_start();
		break;
	case CTU_DLGITEM_Replay:
		ctud("**REPLAYING RECORDING\n");
		input_replay = 1;
		break;
	case CTU_DLGITEM_Recover:
		p2_block = 1;
	default:
		ctud("**CONTROLLING P1\n");
	}

	refill_health = 0;

	switch (dlgval(CTU_IDX_HEALTH)) {
	case CTU_DLGITEM_Refill:
		refill_health = 1;
		break;
	}

	refill_meter = 0;

	switch (dlgval(CTU_IDX_METER)) {
	case CTU_DLGITEM_0_Bars:
		refill_meter = 1;
		break;
	case CTU_DLGITEM_1_Bar:
		refill_meter = 2;
		break;
	case CTU_DLGITEM_2_Bars:
		refill_meter = 3;
		break;
	case CTU_DLGITEM_3_Bars:
		refill_meter = 4;
		break;
	}

	refill_magic = deplete_magic = 0;

	switch (dlgval(CTU_IDX_MAGIC)) {
	case CTU_DLGITEM_Refill:
		refill_magic = 1;
		break;
	case CTU_DLGITEM_Deplete:
		deplete_magic = 1;
		break;
	}

	switch (dlgval(CTU_IDX_INFO)) {
	case CTU_DLGITEM_Off:
		deactivate_infodlg();
		break;

	default:
		activate_infodlg();
	}
}

static int round_no = -1;

static int
isfighting(void)
{
	return round_no >= 0;
}

static int
should_refill_health(int state)
{
	switch (state & STATE_STUN_MASK) {
	case 0:
		return 1;
	}
	return 0;
}

static int
is_attacking(int state)
{
	switch (state & STATE_STUN_MASK) {
	/* note that this is the same as STATE_STUN_GBOUNCE, but for our usages,
	 * it shouldn't matter if we mistake one for the other */
	case STATE_STUN_ATTACK:
		return 1;
	}
	return 0;
}

static void
handle_health(void)
{
	static int p1_last_state, p2_last_state;
	int p1_state, p2_state;
	int p1_refill_ok = 0, p2_refill_ok = 0;

	if (!refill_health && !refill_meter && !refill_magic && !infodlg_on) {
		return;
	}

	ReadProcessMemory(proc, P1_STATE_VADDR, &p1_state, 4, NULL);
	ReadProcessMemory(proc, P2_STATE_VADDR, &p2_state, 4, NULL);
	if (p1_state != p1_last_state) {
		ctud("p1 state: 0x%x\n", (unsigned)p1_state);
	}
	if (p2_state != p2_last_state) {
		ctud("                p2 state: 0x%x\n", (unsigned)p2_state);
	}
	p2_last_state = p2_state;
	p1_last_state = p1_state;

	{
		/* only refill meter/magic if we haven't attacked recently, so we don't
		 * refill instantly when someone starts a combo with meter or magic */
		static const int cooldown_timer = 20;
		static int p1_refill_cooldown, p2_refill_cooldown;

		if (is_attacking(p1_state)) {
			p1_refill_cooldown = cooldown_timer;
		} else {
			if (p1_refill_cooldown < 1) {
				p1_refill_ok = 1;
			} else {
				p1_refill_cooldown--;
			}
		}

		if (is_attacking(p2_state)) {
			p2_refill_cooldown = cooldown_timer;
		} else {
			if (p2_refill_cooldown < 1) {
				p2_refill_ok = 1;
			} else {
				p2_refill_cooldown--;
			}
		}
	}

	if (infodlg_on) {
		combo_update(&comboinfo_p1);
		combo_update(&comboinfo_p2);
	}

	if (should_refill_health(p1_state)) {
		if (refill_health) {
			int max;
			int cur;
			ReadProcessMemory(proc, P1_MAX_HP_VADDR, &max, sizeof(max), NULL);
			ReadProcessMemory(proc, P1_HP_VADDR, &cur, sizeof(max), NULL);
			if (cur != max) {
				ctud("p2 combo damage: %d/%d (%.1f%%)\n", (max - cur), max,
				       (100.0*(float)(max-cur))/((float)max));
				WriteProcessMemory(proc, P1_HP_VADDR, &max, sizeof(max), NULL);
			}
		}
		/* remember, fill the _opponent_ super/magic meter when we are recovered */
		if (refill_meter && p2_refill_ok) {
			unsigned char bars;
			unsigned char part;
			ReadProcessMemory(proc, P2_SUPER_BARS_VADDR, &bars, sizeof(bars), NULL);
			ReadProcessMemory(proc, P2_SUPER_PARTIAL_VADDR, &part, sizeof(part), NULL);
			if (bars != (refill_meter-1) || part != 0) {
				bars = refill_meter - 1;
				part = 0;
				WriteProcessMemory(proc, P2_SUPER_BARS_VADDR, &bars, sizeof(bars), NULL);
				WriteProcessMemory(proc, P2_SUPER_PARTIAL_VADDR, &part, sizeof(part), NULL);
			}
		}
		if (refill_magic && p2_refill_ok) {
			int16_t magic;
			ReadProcessMemory(proc, P2_MAGIC_VADDR, &magic, sizeof(magic), NULL);
			if (magic != p2_magic_max) {
				WriteProcessMemory(proc, P2_MAGIC_VADDR, &p2_magic_max, sizeof(p2_magic_max), NULL);
			}
		}
		if (deplete_magic && p2_refill_ok) {
			int16_t magic;
			ReadProcessMemory(proc, P2_MAGIC_VADDR, &magic, sizeof(magic), NULL);
			if (magic != 0) {
				magic = 0;
				WriteProcessMemory(proc, P2_MAGIC_VADDR, &magic, sizeof(magic), NULL);
			}
		}
		combo_reset(&comboinfo_p2);
	}
	if (should_refill_health(p2_state)) {
		if (refill_health) {
			int max;
			int cur;
			ReadProcessMemory(proc, P2_MAX_HP_VADDR, &max, sizeof(max), NULL);
			ReadProcessMemory(proc, P2_HP_VADDR, &cur, sizeof(max), NULL);
			if (cur != max) {
				ctud("p1 combo damage: %d/%d (%.1f%%)\n", (max - cur), max,
				       (100.0*(float)(max-cur))/((float)max));
				WriteProcessMemory(proc, P2_HP_VADDR, &max, sizeof(max), NULL);
			}
		}
		/* remember, fill the _opponent_ super/magic meter when we are recovered */
		if (refill_meter && p1_refill_ok) {
			unsigned char bars;
			unsigned char part;
			ReadProcessMemory(proc, P1_SUPER_BARS_VADDR, &bars, sizeof(bars), NULL);
			ReadProcessMemory(proc, P1_SUPER_PARTIAL_VADDR, &part, sizeof(part), NULL);
			if (bars != (refill_meter-1) || part != 0) {
				bars = refill_meter - 1;
				part = 0;
				WriteProcessMemory(proc, P1_SUPER_BARS_VADDR, &bars, sizeof(bars), NULL);
				WriteProcessMemory(proc, P1_SUPER_PARTIAL_VADDR, &part, sizeof(part), NULL);
			}
		}
		if (refill_magic && p1_refill_ok) {
			int16_t magic;
			ReadProcessMemory(proc, P1_MAGIC_VADDR, &magic, sizeof(magic), NULL);
			if (magic != p1_magic_max) {
				WriteProcessMemory(proc, P1_MAGIC_VADDR, &p1_magic_max, sizeof(p1_magic_max), NULL);
			}
		}
		if (deplete_magic && p1_refill_ok) {
			int16_t magic;
			ReadProcessMemory(proc, P1_MAGIC_VADDR, &magic, sizeof(magic), NULL);
			if (magic != 0) {
				magic = 0;
				WriteProcessMemory(proc, P1_MAGIC_VADDR, &magic, sizeof(magic), NULL);
			}
		}
		combo_reset(&comboinfo_p1);
	}
}

static int
handle_p1_input(int val)
{
	static int last_val;
	static int just_swapped;
	int mylastval = last_val;

	last_val = val;

	if (isfighting()) {
		handle_health();
	} else {
		if (!input_swapped) {
			static int choosing_char;
			int color;
			ReadProcessMemory(proc, P1_CHAR_COLOR_VADDR, &color, sizeof(color), NULL);
			if (!choosing_char && color < 0) {
				choosing_char = 1;
			}
			if (choosing_char && color >= 0) {
				input_swapped = 1;
				just_swapped = 1;
				last_val = mylastval = val;
			}
		}
	}

	if (isfighting()) {
		if ((val & CTRL_PAUSE) && !(mylastval & CTRL_PAUSE)) {
			if (pausemenu_active) {
				deactivate_pausemenu();
			} else {
				activate_pausemenu();
			}
			if (isfighting()) {
				ctud("we are fighting; sending pause to fm2k\n");
				return CTRL_PAUSE;
			} else {
				ctud("not fighting; swallowed pause\n");
				return 0;
			}
		}

		if ((val & CTRL_PAUSE)) {
			val &= ~CTRL_PAUSE;
		}

		if (pausemenu_active) {
			if (p1_reverse) {
				val = translate(val);
			}
			pause_ctrl(val);
			return 0;
		}
	}

	if (input_swapped) {
		if (just_swapped && val == mylastval) {
			val = 0;
		} else {
			just_swapped = 0;
		}
		p2_input = translate(val);
		output_ctrl(p2_input);
		return 0;
	}

	output_ctrl(val);

	return val;
}

static int
block_input(int state, int *atech)
{
	int val = 0;

	*atech = 0;

	switch ((state & STATE_STUN_MASK)) {
	case STATE_STUN_HIT:
		/* we are in hitstun; hold back to try to block */
		val = CTRL_BACK;
		*atech = 1;
		break;

	case STATE_STUN_BLOCK:
		/* hitstun; try to block */
		val = CTRL_BACK;
		break;

	case STATE_STUN_GBOUNCE:
		/* try to ground tech */
		val = CTRL_BACK | CTRL_DOWN;
		break;
	}
	return val;
}

static int
handle_p2_input(int val)
{
	if (input_swapped) {
		val = p2_input;
	}
	if (recording) {
		recording_rec(val);

	} else if (input_replay) {
		val = recording_play();

	} else if (p2_block) {
		static int p2_last_state, block_cooldown, block_cooldown_val;
		int p2_state;
		int tech;
		unsigned int p2_height = HEIGHT_GROUNDED;

		ReadProcessMemory(proc, P2_STATE_VADDR, &p2_state, 4, NULL);

		val = block_input(p2_state, &tech);
		if (tech) {
			/* if we are in the air, hold 'up' to air tech if we can */
			ReadProcessMemory(proc, P2_HEIGHT_VADDR, &p2_height, sizeof(p2_height), NULL);
			if (p2_height != HEIGHT_GROUNDED) {
				val |= CTRL_UP;
			}
		}

		if (!val) {
			if (block_cooldown) {
				block_cooldown--;
				if (!block_cooldown) {
					ctud(" == cooldown ending\n");
				}
				val = block_cooldown_val;

			} else {
				val = block_input(p2_last_state, &tech);
				if (val) {
					block_cooldown = 30;
					block_cooldown_val = val;
				}
			}
		}

		p2_last_state = p2_state;
	}
	return val;
}

static void
round_end(void)
{
	ctud("\nround_end\n");
	switch_state("CONTROLLING P1");
	recording_start();
	recording_rewind();
	recording = input_swapped = 0;
}

static int
select_stage(void)
{
#if 0
	SYSTEMTIME st;
	FILETIME ft;

	GetSystemTime(&st);
	SystemTimeToFileTime(&st, &ft);

	srand(ft.dwLowDateTime);

	return rand() % 4;
#else
	/* just always return rarity's stage for now, to ensure we minimize
	 * performance issues */
	return 0;
#endif
}

static void
mainthread_update_input(void)
{
	if (!PostThreadMessage(mainthread, CTU_THREADUPDATE, 0, 0)) {
		ctud("PostThreadMessage (threadupdate) %d\n", (int)GetLastError());
	}
}

static int stats_dirty;

static void
stat_health(void *haddr, void *maxhaddr, struct info_val *val)
{
	int health;
	ReadProcessMemory(proc, haddr, &health, sizeof(health), NULL);
	if (val->val[0] != health) {
		val->val[0] = health;
		val->dirty = 1;
		stats_dirty = 1;
	}

	if (!val->val[1]) {
		ReadProcessMemory(proc, maxhaddr, &val->val[1], sizeof(val->val[1]), NULL);
		val->dirty = 1;
		stats_dirty = 1;
	}
}

static void
stat_meter(void *meter_addr, void *partial_addr, void *maxp_addr,
           struct info_val *val)
{
	unsigned char bars;
	unsigned char partial;
	unsigned char max;
	float fval;

	ReadProcessMemory(proc, meter_addr, &bars, sizeof(bars), NULL);
	ReadProcessMemory(proc, partial_addr, &partial, sizeof(partial), NULL);
	if (!val->val[0]) {
		ReadProcessMemory(proc, maxp_addr, &max, sizeof(max), NULL);
		val->val[0] = max;
	} else {
		max = val->val[0];
	}

	fval = bars + ((float)partial/(float)val->val[0]);

	if (fval != val->fval) {
		val->fval = fval;
		val->dirty = 1;
		stats_dirty = 1;
	}
}

static void
stat_magic(void *magic_addr, int max, struct info_val *val)
{
	int16_t magic;

	ReadProcessMemory(proc, magic_addr, &magic, sizeof(magic), NULL);

	if (max == 6) {
	} else {
		max = 3;
		magic *= -1;
	}

	if (magic != val->val[0]) {
		val->val[0] = magic;
		val->dirty = 1;
		stats_dirty = 1;
	}
	if (max != val->val[1]) {
		val->val[1] = max;
		val->dirty = 1;
		stats_dirty = 1;
	}
}

static void
stat_combo(struct comboinfo *cinfo)
{
	if (info_curc_dmg.val[0] != cinfo->combo_health) {
		info_curc_dmg.val[0] = cinfo->combo_health;
		info_curc_dmg.dirty = 1;
		stats_dirty = 1;
	}

	if (info_curc_meter.fval != cinfo->combo_meter) {
		info_curc_meter.fval = cinfo->combo_meter;
		info_curc_meter.dirty = 1;
		stats_dirty = 1;
	}

	if (info_curc_magic.val[0] != cinfo->combo_magic) {
		info_curc_magic.val[0] = cinfo->combo_magic;
		info_curc_magic.dirty = 1;
		stats_dirty = 1;
	}

	if (info_curc_dmg.val[0] > info_maxc_dmg.val[0]) {
		info_maxc_dmg.val[0] = info_curc_dmg.val[0];
		info_maxc_dmg.dirty = 1;
		stats_dirty = 1;

		if (info_maxc_meter.fval != info_curc_meter.fval) {
			info_maxc_meter.fval = info_curc_meter.fval;
			info_maxc_meter.dirty = 1;
		}
		if (info_maxc_magic.val[0] != info_curc_magic.val[0]) {
			info_maxc_magic.val[0] = info_curc_magic.val[0];
			info_maxc_magic.dirty = 1;
		}
	}
}

static void
update_stats(void)
{
	if (!infodlg_on) {
		return;
	}

	stats_dirty = 0;

	stat_health(P1_HP_VADDR, P1_MAX_HP_VADDR, &info_p1_health);
	stat_health(P2_HP_VADDR, P2_MAX_HP_VADDR, &info_p2_health);

	stat_meter(P1_SUPER_BARS_VADDR, P1_SUPER_PARTIAL_VADDR, P1_SUPER_PARTIAL_MAX_VADDR, &info_p1_meter);
	stat_meter(P2_SUPER_BARS_VADDR, P2_SUPER_PARTIAL_VADDR, P2_SUPER_PARTIAL_MAX_VADDR, &info_p2_meter);

	stat_magic(P1_MAGIC_VADDR, p1_magic_max, &info_p1_magic);
	stat_magic(P2_MAGIC_VADDR, p2_magic_max, &info_p2_magic);

	if (comboinfo_p1.combo_health > 0 || comboinfo_p2.combo_health > 0) {
		if (comboinfo_p1.combo_health >= comboinfo_p2.combo_health) {
			stat_combo(&comboinfo_p1);
		} else {
			stat_combo(&comboinfo_p2);
		}
	}

	if (stats_dirty) {
		if (!PostThreadMessage(mainthread, CTU_INFOUPDATE, 0, 0)) {
			ctud("PostThreadMessage (INFOUPDATE) error %d\n", (int)GetLastError());
		}
	}
}

static int16_t
character_max_magic(int charid)
{
	switch (charid) {
	case FIM_CHAR_TWILIGHT:
	case FIM_CHAR_APPLEJACK:
		return -3;

	case FIM_CHAR_RARITY:
	case FIM_CHAR_PINKIE:
		return 6;

	default:
		ctud("warning got weird character id 0x%x\n", (unsigned)charid);
		return 0;
	}
}

static int bg_volume;
static int se_volume;

static unsigned int
handle_exc(DEBUG_EVENT *de)
{
	if (de->u.Exception.ExceptionRecord.ExceptionCode == EXCEPTION_ACCESS_VIOLATION) {
		ctud("Access Violation %08x at %08x: %s@%08x\n",
		     (unsigned)de->u.Exception.ExceptionRecord.ExceptionCode,
		     (unsigned)de->u.Exception.ExceptionRecord.ExceptionAddress,
		     de->u.Exception.ExceptionRecord.ExceptionInformation[0] ? "read" : "write",
		     (unsigned)de->u.Exception.ExceptionRecord.ExceptionInformation[1]);
		return DBG_EXCEPTION_NOT_HANDLED;
	}

	if (de->u.Exception.ExceptionRecord.ExceptionCode == EXCEPTION_BREAKPOINT) {
		unsigned long address;
		CONTEXT c;

		if (de->dwThreadId != proc_thread_id) {
			ctud("bad thread id: %d != %d\n", (int)de->dwThreadId, (int)proc_thread_id);
			proc_thread_id = de->dwThreadId;
			proc_thread = OpenThread(THREAD_ALL_ACCESS, FALSE, proc_thread_id);
			mainthread_update_input();
			if (!proc_thread) {
				ctud("OpenThread returned error %d\n", (int)GetLastError());
			}
			return DBG_CONTINUE;
		}

		address = (unsigned long)de->u.Exception.ExceptionRecord.ExceptionAddress;

		switch (address) {
		case STAGE_SELECT_BREAK:
			c.ContextFlags = CONTEXT_INTEGER;
			GetThreadContext(proc_thread, &c);
			c.Eax = select_stage();
			SetThreadContext(proc_thread, &c);
			break;

		case ROUND_START_BREAK:
			ctud("new round\n");
			round_no++;
			input_swapped = 0;
			p2_block = 1;
			{
				unsigned int charid;
				ReadProcessMemory(proc, P1_CHAR_VADDR, &charid, sizeof(charid), NULL);
				p1_magic_max = character_max_magic(charid);

				ReadProcessMemory(proc, P2_CHAR_VADDR, &charid, sizeof(charid), NULL);
				p2_magic_max = character_max_magic(charid);
			}
			break;

		case ROUND_END_BREAK: {
			DWORD mem;

			ReadProcessMemory(proc, ROUND_ESI_VADDR, &mem, 4, NULL);

			c.ContextFlags = CONTEXT_INTEGER;
			GetThreadContext(proc_thread, &c);

			c.Esi = mem;

			if (c.Eax == c.Edx) {
				round_no = -1;
			} else if (c.Eax == c.Esi) {
				round_no = -1;
			}

			SetThreadContext(proc_thread, &c);

			round_end();
			break;
		}

		case VS_P1_KEY_BREAK:
			c.ContextFlags = CONTEXT_INTEGER;
			GetThreadContext(proc_thread, &c);

			c.Eax = handle_p1_input(c.Eax);

			WriteProcessMemory(proc, P1_INPUT_VADDR, &c.Eax, 4, NULL);
			if (0) {
				static int last;
				if (last != c.Eax) {
					/*ctud(" === p1 input %x\n", (unsigned)c.Eax);*/
				}
				last = c.Eax;
			}
			SetThreadContext(proc_thread, &c);
			break;

		case VS_P2_KEY_BREAK:
			c.ContextFlags = CONTEXT_INTEGER;
			GetThreadContext(proc_thread, &c);

			c.Eax = handle_p2_input(c.Eax);

			WriteProcessMemory(proc, P2_INPUT_VADDR, &c.Eax, 4, NULL);
			if (0) {
				static int last;
				if (last != c.Eax) {
					/*ctud(" === p2 input %x\n", (unsigned)c.Eax);*/
				}
				last = c.Eax;
			}
			SetThreadContext(proc_thread, &c);
			break;

		case SE_VOLUME_BREAK:
			c.ContextFlags = CONTEXT_INTEGER;
			GetThreadContext(proc_thread, &c);
			c.Edx = se_volume;
			SetThreadContext(proc_thread, &c);
			break;
		case BGM_VOLUME_BREAK:
			c.ContextFlags = CONTEXT_INTEGER;
			GetThreadContext(proc_thread, &c);
			c.Edx = bg_volume;
			SetThreadContext(proc_thread, &c);
			break;

		case FRAME_BREAK: {
			static int frame_counter;

			c.ContextFlags = CONTEXT_INTEGER;
			GetThreadContext(proc_thread, &c);
			c.Esi = c.Eax;
			SetThreadContext(proc_thread, &c);

			if (frame_counter == 10) {
				fimwindow();
				activate_infodlg();
			}
			if (frame_counter > 10) {
				if (round_no >= 0) {
					update_stats();
				}
				frame_counter = 0;
			}
			frame_counter++;
			break;
		}

		default:
			ctud("unhandled breakpoint: 0x%lx\n", address);
#if 0
			{
				unsigned char nop = 0x90;
				CONTEXT c;

				c.ContextFlags = CONTEXT_FULL;
				GetThreadContext(proc_thread, &c);
				c.Eip--;
				WriteProcessMemory(proc, (void *)c.Eip, &nop, 1, NULL);
				FlushInstructionCache(proc, NULL, 0);
			}
#endif
		}
	}
	return DBG_CONTINUE;
}

static void
inject_hooks(void)
{
	/* frame draw callback */
	WriteProcessMemory(proc, FRAME_CADDR, FRAME_CODE, sizeof(FRAME_CODE), NULL);

	/* set stage */
	WriteProcessMemory(proc, STAGE_SELECT_CADDR, STAGE_SELECT_CODE, sizeof(STAGE_SELECT_CODE), NULL);

	/* record and alter player inputs */
	WriteProcessMemory(proc, VS_P1_KEY_CADDR, VS_P1_KEY_CODE, sizeof(VS_P1_KEY_CODE), NULL);
	WriteProcessMemory(proc, VS_P2_KEY_CADDR, VS_P2_KEY_CODE, sizeof(VS_P2_KEY_CODE), NULL);

	/* notice when a round starts */
	WriteProcessMemory(proc, ROUND_START_CADDR, ROUND_START_CODE,
	                   sizeof(ROUND_START_CODE), NULL);
	/* notice when a new round starts */
	WriteProcessMemory(proc, ROUND_END_CADDR, ROUND_END_CODE,
	                   sizeof(ROUND_END_CODE), NULL);

	if (bg_volume || se_volume) {
		WriteProcessMemory(proc, VOLUME_SET_1_CADDR, VOLUME_SET_1_CODE, sizeof(VOLUME_SET_1_CODE), NULL);
		WriteProcessMemory(proc, VOLUME_SET_2_CADDR, VOLUME_SET_2_CODE, sizeof(VOLUME_SET_2_CODE), NULL);
	}
}

static DWORD WINAPI
runproc(LPVOID lpParam)
{
	char path[] = "FightingIsMagic.exe";
	/*char path[] = "K:\\games\\mlp\\FightingIsMagic\\FightingIsMagic.exe";*/
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DEBUG_EVENT de;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	if (!CreateProcess(NULL, path /* command line */,
	                         NULL /* process security attrs */,
	                         NULL /* thread security attrs */,
	                         FALSE /* child inherits handles? */,
									 DEBUG_PROCESS /* flags */,
	                         NULL /* environment */,
	                         NULL /* cwd */,
	                         &si, &pi)) {
		ctud("Could not CreateProcess, error %d\n", (int)GetLastError());
		exit(1);
	}

	proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE /* inherit handles? */,
	                   pi.dwProcessId);
	if (!proc) {
		ctud("Could not OpenProcess, error %d\n", (int)GetLastError());
		exit(1);
	}

	while (WaitForDebugEvent(&de, INFINITE)) {
		unsigned int state = DBG_EXCEPTION_NOT_HANDLED;
		switch (de.dwDebugEventCode) {
		case EXCEPTION_DEBUG_EVENT:
			state = handle_exc(&de);
			
		case CREATE_THREAD_DEBUG_EVENT:
			break;

		case CREATE_PROCESS_DEBUG_EVENT:
			proc_thread = de.u.CreateProcessInfo.hThread;
			proc_thread_id = de.dwThreadId;
			mainthread_update_input();
			inject_hooks();
			break;

		case EXIT_THREAD_DEBUG_EVENT:
			break;

		case EXIT_PROCESS_DEBUG_EVENT:
			goto done;

		case LOAD_DLL_DEBUG_EVENT:
			break;

		case UNLOAD_DLL_DEBUG_EVENT:
			break;

		case OUTPUT_DEBUG_STRING_EVENT: {
			char *str = malloc(de.u.DebugString.nDebugStringLength);
			ReadProcessMemory(proc, de.u.DebugString.lpDebugStringData, str,
			                  de.u.DebugString.nDebugStringLength, NULL);
			ctud(" = OUTPUT_DEBUG_STRING_EVENT(%s)\n", str);
			free(str);
			break;
		}
			
		case RIP_EVENT:
			ctud(" = RIP_EVENT\n");
			break;

		default:
			ctud(" = unhandled de.dwDebugEventCode %d\n", (int)de.dwDebugEventCode);
		}

		ContinueDebugEvent(de.dwProcessId, de.dwThreadId, state);
	}

 done:
	PostThreadMessage(mainthread, WM_QUIT, 0, 0);

	return 0;
}

int WINAPI
WinMain(HINSTANCE hinstance, HINSTANCE hPrevInstance,
        LPSTR lpCmdLine, int nCmdShow) 
{
	LPWSTR *argv;
	int argc, i;
	argv = CommandLineToArgvW(GetCommandLineW(), &argc);
	for (i = 1; i < argc; i++) {
		if (StrCmpW(argv[i], L"-stdout") == 0) {
			i++;
			if (i >= argc) {
				ctud("-stdout takes an argument\n");
				return 1;
			}
			if (!_wfreopen(argv[i], L"w", stdout)) {
				return 2;
			}
			continue;
		}
		if (StrCmpW(argv[i], L"-stderr") == 0) {
			i++;
			if (i >= argc) {
				ctud("-stderr takes an argument\n");
				return 1;
			}
			if (!_wfreopen(argv[i], L"w", stderr)) {
				return 2;
			}
			continue;
		}
		if (StrCmpW(argv[i], L"-bgvol") == 0) {
			i++;
			if (i >= argc) {
				ctud("-bgvol takes an argument\n");
				return 1;
			}
			if (!StrToIntExW(argv[i], 0, &bg_volume)) {
				ctud("Invalid -bgvol integer specified (error %d)\n", (int)GetLastError());
				return 1;
			}
			continue;
		}
		if (StrCmpW(argv[i], L"-sevol") == 0) {
			i++;
			if (i >= argc) {
				ctud("-bgvol takes an argument\n");
				return 1;
			}
			if (!StrToIntExW(argv[i], 0, &se_volume)) {
				ctud("Invalid -sevol integer specified (error %d)\n", (int)GetLastError());
				return 1;
			}
			continue;
		}
		if (StrCmpW(argv[i], L"-vol") == 0) {
			i++;
			if (i >= argc) {
				ctud("-vol takes an argument\n");
				return 1;
			}
			if (!StrToIntExW(argv[i], 0, &bg_volume)) {
				ctud("Invalid -vol integer specified (error %d)\n", (int)GetLastError());
				return 1;
			}
			se_volume = bg_volume;
			continue;
		}
		if (StrCmpW(argv[i], L"-mutebg") == 0) {
			bg_volume = -10000;
			continue;
		}
		if (StrCmpW(argv[i], L"-mutese") == 0) {
			se_volume = -10000;
			continue;
		}
		if (StrCmpW(argv[i], L"-mute") == 0) {
			bg_volume = se_volume = -10000;
			continue;
		}
		ctud("Invalid option '%S' given\n", argv[i]);
		return 1;
	}

	hinst = hinstance;
	mainthread = GetCurrentThreadId();

	if (!CreateThread(NULL /* attrs*/,
	                  0 /* stack size */,
	                  &runproc,
	                  NULL /* param */,
	                  0 /* flags */,
	                  NULL /* thread id */)) {
		ctud("CreateThread error %d\n", (int)GetLastError());
		return 1;
	}

	return menu_loop();
}
