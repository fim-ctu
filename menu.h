#ifndef CTU_MENU_H
#define CTU_MENU_H

struct dlg_item {
	const char *name;
	int val;
};

#define CTU_IDX_ACTION (0)
#define CTU_IDX_HEALTH (1)
#define CTU_IDX_METER  (2)
#define CTU_IDX_MAGIC  (3)
#define CTU_IDX_INFO   (4)

#define CTU_DLGITEM_None    (0)
#define CTU_DLGITEM_Recover (1)
#define CTU_DLGITEM_Control (2)
#define CTU_DLGITEM_Record  (3)
#define CTU_DLGITEM_Replay  (4)
#define CTU_DLGITEM_Refill  (5)
#define CTU_DLGITEM_On      (6)
#define CTU_DLGITEM_Off     (7)
#define CTU_DLGITEM_Normal  (8)
#define CTU_DLGITEM_Deplete (9)
#define CTU_DLGITEM_0_Bars  (10)
#define CTU_DLGITEM_1_Bar   (11)
#define CTU_DLGITEM_2_Bars  (12)
#define CTU_DLGITEM_3_Bars  (13)

struct ctu_dlg {
	const char *label;
	struct dlg_item *val_arr;
	int cur;
};

extern struct ctu_dlg dlg[];
extern int n_dlg;

extern int menu_loop(void);

extern int get_menu_coords(RECT *rc_win, RECT *rc_client);

#endif /* CTU_MENU_H */
