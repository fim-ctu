#include <stdio.h>

#include "ctu.h"
#include "inject.h"

static struct {
	const char *str;
	int resource;
} labels[] = {
	{ "P1",            CTU_INF_CLBL + 0 },
	{ "P2",            CTU_INF_RLBL + 0 },
	{ "Health:",       CTU_INF_LLBL + 1 },
	{ "Meter:",        CTU_INF_LLBL + 2 },
	{ "Magic:",        CTU_INF_LLBL + 3 },
	{ "Current",       CTU_INF_CLBL + 5 },
	{ "Max",           CTU_INF_RLBL + 5 },
	{ "Damage:",       CTU_INF_LLBL + 6 },
	{ "Meter:",        CTU_INF_LLBL + 7 },
	{ "Magic:",        CTU_INF_LLBL + 8 },
	{ NULL, 0 },
};

struct info_val info_p1_health = { .res = CTU_INF_CLBL + 1, };
struct info_val info_p2_health = { .res = CTU_INF_RLBL + 1, };

struct info_val info_p1_meter = { .res = CTU_INF_CLBL + 2, .isfloat = 1, };
struct info_val info_p2_meter = { .res = CTU_INF_RLBL + 2, .isfloat = 1, };

struct info_val info_p1_magic = { .res = CTU_INF_CLBL + 3, };
struct info_val info_p2_magic = { .res = CTU_INF_RLBL + 3, };

struct info_val info_curc_dmg = { .res = CTU_INF_CLBL + 6, };
struct info_val info_maxc_dmg = { .res = CTU_INF_RLBL + 6, };

struct info_val info_curc_meter = { .res = CTU_INF_CLBL + 7, .isfloat = 1, .isplus = 1, };
struct info_val info_maxc_meter = { .res = CTU_INF_RLBL + 7, .isfloat = 1, .isplus = 1, };

struct info_val info_curc_magic = { .res = CTU_INF_CLBL + 8, .isplus = 1, };
struct info_val info_maxc_magic = { .res = CTU_INF_RLBL + 8, .isplus = 1, };

static struct info_val *vals[] = {
	&info_p1_health,
	&info_p2_health,
	&info_p1_meter,
	&info_p2_meter,
	&info_p1_magic,
	&info_p2_magic,
	&info_curc_dmg,
	&info_maxc_dmg,
	&info_curc_meter,
	&info_maxc_meter,
	&info_curc_magic,
	&info_maxc_magic,
	NULL,
};

static HANDLE icon_arrow_l;
static HANDLE icon_arrow_ul;
static HANDLE icon_arrow_u;
static HANDLE icon_arrow_ur;
static HANDLE icon_arrow_r;
static HANDLE icon_arrow_dr;
static HANDLE icon_arrow_d;
static HANDLE icon_arrow_dl;

static HANDLE icon_atk_a;
static HANDLE icon_atk_b;
static HANDLE icon_atk_c;
static HANDLE icon_atk_d;

void
info_update(HWND hwnd)
{
	int i;
	for (i = 0; vals[i]; i++) {
		static char buf[64];
		struct info_val *val = vals[i];

		if (val->dirty) {
			val->dirty = 0;
		} else {
			continue;
		}

		if (val->isfloat) {
			if (val->isplus) {
				snprintf(buf, sizeof(buf), "%+.2f", val->fval);
			} else {
				snprintf(buf, sizeof(buf), "%.2f", val->fval);
			}
		} else if (val->val[1]) {
			if (val->isplus) {
				snprintf(buf, sizeof(buf), "%+d/%d", val->val[0], val->val[1]);
			} else {
				snprintf(buf, sizeof(buf), "%d/%d", val->val[0], val->val[1]);
			}
		} else {
			if (val->isplus) {
				snprintf(buf, sizeof(buf), "%+d", val->val[0]);
			} else {
				snprintf(buf, sizeof(buf), "%d", val->val[0]);
			}
		}

		if (IsWindow(hwnd) && !SetDlgItemText(hwnd, val->res, buf)) {
			ctud("SetDlgItemText (res %d) error %d\n", val->res, (int)GetLastError());
		}
	}
}

static HANDLE
loadimg(HINSTANCE hinst, int id)
{
	return LoadImage(hinst, MAKEINTRESOURCE(id), IMAGE_ICON, 0 /*x*/, 0 /*y*/, LR_SHARED /*flags*/);
}

static const int draw_off_x = 30;
static const int draw_off_y = 210;

static void
set_arrow_img(HDC hdc, int row, HANDLE icon)
{
	int x, y;

	x = draw_off_x;
	y = draw_off_y + row * 20;
	if (!DrawIconEx(hdc, x, y, icon, 0 /*width*/, 0 /*height*/, 0 /*frame*/, NULL /*brush*/, DI_NORMAL)) {
		ctud("DrawIcon error %d\n", (int)GetLastError());
	}
}

static void
set_button_img(HDC hdc, int col, int row, HANDLE icon)
{
	int x, y;

	x = draw_off_x + 30 + col * 20;
	y = draw_off_y + row * 20 ;
	if (!DrawIconEx(hdc, x, y, icon, 0 /*width*/, 0 /*height*/, 0 /*frame*/, NULL /*brush*/, DI_NORMAL)) {
		ctud("DrawIcon error %d\n", (int)GetLastError());
	}
}

static void
set_input_img(HDC hdc, int index, unsigned int ctrl)
{
	HANDLE icon = NULL;
	int baseindex = 0;

	switch (ctrl & 0xF) {
	case CTRL_BACK:
		icon = icon_arrow_l;
		break;

	case CTRL_BACK | CTRL_UP:
		icon = icon_arrow_ul;
		break;

	case CTRL_UP:
		icon = icon_arrow_u;
		break;

	case CTRL_UP | CTRL_FWD:
		icon = icon_arrow_ur;
		break;

	case CTRL_FWD:
		icon = icon_arrow_r;
		break;

	case CTRL_FWD | CTRL_DOWN:
		icon = icon_arrow_dr;
		break;

	case CTRL_DOWN:
		icon = icon_arrow_d;
		break;

	case CTRL_DOWN | CTRL_BACK:
		icon = icon_arrow_dl;
		break;
	}

	if (icon) {
		set_arrow_img(hdc, index, icon);
	}

	if ((ctrl & CTRL_A)) {
		set_button_img(hdc, baseindex++, index, icon_atk_a);
	}
	if ((ctrl & CTRL_B)) {
		set_button_img(hdc, baseindex++, index, icon_atk_b);
	}
	if ((ctrl & CTRL_C)) {
		set_button_img(hdc, baseindex++, index, icon_atk_c);
	}
	if ((ctrl & CTRL_D)) {
		set_button_img(hdc, baseindex++, index, icon_atk_d);
	}

}

#define NINPUTS (14)
static int input_buf[NINPUTS];

void
info_input(HWND hwnd, int ctrl)
{
	int i;
	HDC hdc;
	RECT rect;

	/* adjust values */
	memmove(&input_buf[1], &input_buf[0], sizeof(input_buf[0]) * (NINPUTS-1));
	input_buf[0] = ctrl;

	/* drawing boilerplate */
	hdc = GetDC(hwnd);
	if (!hdc) {
		ctud("Error from GetDC: %d\n", (int)GetLastError());
		return;
	}

	if (!GetClientRect(hwnd, &rect)) {
		ctud("GetClientRect error %d\n", (int)GetLastError());
		return;
	}

	rect.left = draw_off_x;
	rect.top = draw_off_y;

	/* clear the draw area */
	if (!FillRect(hdc, &rect, (HBRUSH)(COLOR_3DFACE + 1))) {
		ctud("FillRect error: %d\n", (int)GetLastError());
	}

	/* draw values */
	for (i = 0; i < NINPUTS; i++) {
		set_input_img(hdc, i, input_buf[i]);
	}

	ReleaseDC(hwnd, hdc);
}

BOOL CALLBACK
info_dlg_proc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_INITDIALOG: {
		RECT rc;
		int i;

		for (i = 0; labels[i].str; i++) {
			if (!SetDlgItemText(hwnd, labels[i].resource, labels[i].str)) {
				ctud("SetDlgItemText error %d\n", (int)GetLastError());
			}
		}

		if (!SetDlgItemText(hwnd, CTU_INF_LLBL + 9, "Input:")) {
			ctud("SetDlgItemText error %d\n", (int)GetLastError());
		}

		icon_arrow_l  = loadimg(hinst, CTU_ARROW_L);
		icon_arrow_ul = loadimg(hinst, CTU_ARROW_UL);
		icon_arrow_u  = loadimg(hinst, CTU_ARROW_U);
		icon_arrow_ur = loadimg(hinst, CTU_ARROW_UR);
		icon_arrow_r  = loadimg(hinst, CTU_ARROW_R);
		icon_arrow_dr = loadimg(hinst, CTU_ARROW_DR);
		icon_arrow_d  = loadimg(hinst, CTU_ARROW_D);
		icon_arrow_dl = loadimg(hinst, CTU_ARROW_DL);

		icon_atk_a = loadimg(hinst, CTU_ATK_A);
		icon_atk_b = loadimg(hinst, CTU_ATK_B);
		icon_atk_c = loadimg(hinst, CTU_ATK_C);
		icon_atk_d = loadimg(hinst, CTU_ATK_D);

		for (i = 0; vals[i]; i++) {
			vals[i]->dirty = 1;
		}

		if (get_menu_coords(&rc, NULL)) {
			rc.left = rc.right;
			/* actually the width */
			rc.right = 160;
			/* actually the height */
			rc.bottom = rc.bottom - rc.top;

			if (!SetWindowPos(hwnd,
			                  HWND_TOP,
			                  rc.left, rc.top,
			                  rc.right, rc.bottom,
			                  SWP_SHOWWINDOW  | SWP_NOACTIVATE)) {
				ctud("SetWindowPos error %d\n", (int)GetLastError());
			}
		}
	}

	}

	return FALSE;
}

