#include "windows.h"
#include "wingdi.h"

#include "res.h"

#define OFFX 20
#define OFFY 10

define(`LBL',   `RTEXT "", (CTU_CTRL_LBL + $1), OFFX+0, OFFY+eval(20*$1), 45, 10') dnl
define(`LEFT',  `CTEXT "", (CTU_CTRL_LEFT + $1), OFFX+45, OFFY+eval(20*$1), 45, 10') dnl
define(`CENT',  `CTEXT "", (CTU_CTRL_CENT + $1), OFFX+90, OFFY+eval(20*$1), 45, 10') dnl
define(`RIGHT', `CTEXT "", (CTU_CTRL_RIGHT + $1), OFFX+135, OFFY+eval(20*$1), 45, 10') dnl

CTU_MENUDLG DIALOGEX 0, 0, 100, 100
STYLE WS_MINIMIZEBOX | WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_SYSMENU | DS_SHELLFONT
CAPTION " FiM CTU Settings"
FONT 8, "MS Shell Dlg", 0, 0, ANSI_CHARSET
EXSTYLE WS_EX_TOOLWINDOW | WS_EX_NOACTIVATE
BEGIN
	LBL(`0')
	LBL(`1')
	LBL(`2')
	LBL(`3')
	LBL(`4')

	LEFT(`0')
	LEFT(`1')
	LEFT(`2')
	LEFT(`3')
	LEFT(`4')

	CENT(`0')
	CENT(`1')
	CENT(`2')
	CENT(`3')
	CENT(`4')

	RIGHT(`0')
	RIGHT(`1')
	RIGHT(`2')
	RIGHT(`3')
	RIGHT(`4')
END

#undef OFFX
#undef OFFY

#define OFFX 2
#define OFFY 2
#define WIDTH 33

define(`INFL', `RTEXT "", (CTU_INF_LLBL + $1), OFFX+0, OFFY+eval(15*$1), WIDTH, 10')dnl
define(`INFC', `RTEXT "", (CTU_INF_CLBL + $1), OFFX+WIDTH, OFFY+eval(15*$1), WIDTH, 10')dnl
define(`INFR', `RTEXT "", (CTU_INF_RLBL + $1), OFFX+WIDTH*2, OFFY+eval(15*$1), WIDTH, 10')dnl

CTU_INFODLG DIALOGEX 0, 0, 100, 100
STYLE WS_MINIMIZEBOX | WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_SYSMENU | DS_SHELLFONT
CAPTION " Training Mode Info"
FONT 7, "MS Shell Dlg", 0, 0, ANSI_CHARSET
EXSTYLE WS_EX_TOOLWINDOW | WS_EX_NOACTIVATE
BEGIN
	INFL(`1')
	INFL(`2')
	INFL(`3')
	INFL(`6')
	INFL(`7')
	INFL(`8')
	INFL(`9')

	INFC(`0')
	INFC(`1')
	INFC(`2')
	INFC(`3')
	INFC(`5')
	INFC(`6')
	INFC(`7')
	INFC(`8')

	INFR(`0')
	INFR(`1')
	INFR(`2')
	INFR(`3')
	INFR(`5')
	INFR(`6')
	INFR(`7')
	INFR(`8')
END

#undef OFFX
#undef OFFY

CTU_ARROW_L  ICON "inputimgs/arrow_l.ico"
CTU_ARROW_UL ICON "inputimgs/arrow_ul.ico"
CTU_ARROW_U  ICON "inputimgs/arrow_u.ico"
CTU_ARROW_UR ICON "inputimgs/arrow_ur.ico"
CTU_ARROW_R  ICON "inputimgs/arrow_r.ico"
CTU_ARROW_DR ICON "inputimgs/arrow_dr.ico"
CTU_ARROW_D  ICON "inputimgs/arrow_d.ico"
CTU_ARROW_DL ICON "inputimgs/arrow_dl.ico"

CTU_ATK_A ICON "inputimgs/attack_a.ico"
CTU_ATK_B ICON "inputimgs/attack_b.ico"
CTU_ATK_C ICON "inputimgs/attack_c.ico"
CTU_ATK_D ICON "inputimgs/attack_d.ico"
