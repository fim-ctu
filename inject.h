/*
 * The contents of this file originally come from LunaPort and MTSP
 *
 * Since header files and interfaces et al are not copyrightable,
 * this file has no copyright nor license.
 */

#ifndef CTU_INJECT_H
#define CTU_INJECT_H

/* an int3 taking up 6 bytes */
static unsigned char INT3_6[] = {0xCC,0x90,0x90,0x90,0x90,0x90};
static unsigned char INT3_5[] = {0xCC,0x90,0x90,0x90,0x90};
static unsigned char INT3_2[] = {0xCC,0x90};
static unsigned char INT3_1[] = {0xCC};

#define STAGE_SELECT_CADDR ((void*)0x00408756UL)
#define STAGE_SELECT_BREAK (0x00408756UL)
#define STAGE_SELECT_CODE INT3_5

#define VS_P1_KEY_CADDR ((void*)0x0041474AUL)
#define VS_P1_KEY_BREAK (0x0041474AUL)
#define VS_P1_KEY_CODE INT3_5

#define VS_P2_KEY_CADDR ((void*)0x00414764UL)
#define VS_P2_KEY_BREAK (0x00414764UL)
#define VS_P2_KEY_CODE INT3_5

#define ROUND_END_CADDR ((void*)0x00409715UL)
#define ROUND_END_BREAK (0x00409715UL)
#define ROUND_END_CODE INT3_6

#define ROUND_ESI_VADDR ((void*)0x004EDCACUL)

#define ROUND_START_CADDR ((void*)0x0040897FUL)
#define ROUND_START_BREAK (0x0040897FUL)
#define ROUND_START_CODE INT3_1

#define FRAME_CADDR ((void*)0x00404C37UL)
#define FRAME_BREAK (0x00404C37UL)
#define FRAME_CODE INT3_2

/* in general, P2 stuff seems to be stored from P1 stuff at an offset of 0xE03F */

/* where the current input for P1 is stored */
#define P1_INPUT_VADDR ((void*)0x004259C0)
#define P2_INPUT_VADDR ((void*)0x004259C4)

#define P1_HP_VADDR ((void*)0x004DFC85UL)
#define P1_MAX_HP_VADDR ((void*)0x004DFC91UL)
#define P1_STATE_VADDR ((void*)0x0047033EUL)

#define P2_HP_VADDR ((void*)0x004EDCC4UL)
#define P2_MAX_HP_VADDR ((void*)0x004EDCD0UL)
#define P2_STATE_VADDR ((void*)0x004704BCUL)

/* what is our current / last attack? */
/* 1 byte, unsigned; this is the last attack we performed */
/* mids and lows are not differentiated between (this can be determined by
 * looking at char state, and seeing if we're ducking). but low c, for example
 * _is_ different than launcher (low forward c) */
#define P1_ATK_VADDR ((void*)0x4dfcd5UL)
#define P2_ATK_VADDR ((void*)0x4edd14UL)

/* number of bars; 1 for 1 bar, 2 for 2 bars, etc
 * doesn't indicate a partially-full bar */
/* all of these are 1 byte wide */
#define P1_SUPER_BARS_VADDR ((void*)0x4dfc95UL)
/* partial super bar; 1 byte wide */
/* smallest increment seems to be 0x5, but I'm not sure if that's universal */
#define P1_SUPER_PARTIAL_VADDR ((void*)0x4dfc9dUL)
#define P1_SUPER_BARS_MAX_VADDR ((void*)0x4dfc99UL)
#define P1_SUPER_PARTIAL_MAX_VADDR ((void*)0x4dfca1UL)
/* just define the max bars to be the constant 3; no reason to read it all the time */
#define CTU_BARS_MAX (3)

#define P2_SUPER_BARS_VADDR ((void*)0x4edcd4UL)
#define P2_SUPER_PARTIAL_VADDR ((void*)0x4edcdcUL)
#define P2_SUPER_BARS_MAX_VADDR ((void*)0x4edcd8UL)
#define P2_SUPER_PARTIAL_MAX_VADDR ((void*)0x4edce0UL)

/* 'magic' is a signed, 2-byte-wide value */
/* 0 -> no magic

for characters that have max 3 magic (twi/aj):
   -1 -> 1 magic
	-2 -> 2 magic
	-3 -> 3 magic

for characters that have max 6 magic (rarity/pinkie):
	1 -> 1 magic
	...
	6 -> 6 magic
*/
#define P1_MAGIC_VADDR ((void*)0x4dfd1bUL)
#define P2_MAGIC_VADDR ((void*)0x4edd5aUL)

/* character name; a string */
/* I assume this is string used to load the *.player files */
#define P1_CHARNAME_VADDR ((void*)0x4d1d90UL)
#define P2_CHARNAME_VADDR ((void*)0x4dfdcfUL)

/* character index/id; 4 byte signed integer */
/* for FiM:
 0: Applejack
 1: Twilight Sparkle
 2: Rarity
 3: Pinkie Pie
 -1: No character selected
 */
#define P1_CHAR_VADDR ((void*)0x470020UL)
#define P2_CHAR_VADDR ((void*)0x470024UL)

#define FIM_CHAR_APPLEJACK (0)
#define FIM_CHAR_TWILIGHT  (1)
#define FIM_CHAR_RARITY    (2)
#define FIM_CHAR_PINKIE    (3)

/* array of character names, 256 bytes per character */
#define CHAR_NAMES_VADDR ((void*)0x435474UL)
/* array of stage names, 256 bytes per stage */
#define STAGE_NAMES_VADDR ((void*)0x43A29CUL)

/* 4 bytes, in network-byte-order, it looks like, for some reason? */
#define P1_POS_VADDR ((void*)0x4dfcc1UL)
#define P2_POS_VADDR ((void*)0x4edd00UL)
/* 1 byte */
/* if 0x1, our controls are 'reversed'; that is, left is towards, right is back */
#define P1_REVERSE_VADDR ((void*)0x4dfcd1UL)
#define P2_REVERSE_VADDR ((void*)0x4edd10UL)

/* 4 bytes; signed integer. -1 means the player hasn't chosen yet */
#define P1_CHAR_COLOR_VADDR ((void*)0x4dfd8bUL)
#define P2_CHAR_COLOR_VADDR ((void*)0x4eddcaUL)

/* these are NOT CURRENTLY USEFUL (I don't think). They have something to do
 * with air-teching, possibly they represent something with the blue flashing.
 * This is usually not useful to know, since for some cases, we need to be
 * holding 'up' _before_ the flash actually starts */
#define P1_AIRTECH_VADDR ((void*)0x4728f8UL)
#define P2_AIRTECH_VADDR ((void*)0x4703aaUL)

/* these represent how high in the air the player is, either via jumping or
 * getting hit in the air. these are 32-bit unsigned integers. a value of
 * 0x3980000 means we are on the ground, and the values get smaller as we go
 * higher in the air. */
#define P1_HEIGHT_VADDR ((void*)0x4dfcc5UL)
#define P2_HEIGHT_VADDR ((void*)0x4edd04UL)
#define HEIGHT_GROUNDED (0x3980000UL)

#define CTRL_BACK    (0x0001)
#define CTRL_FWD     (0x0002)
#define CTRL_UP      (0x0004)
#define CTRL_DOWN    (0x0008)
#define CTRL_A       (0x0010)
#define CTRL_B       (0x0020)
#define CTRL_C       (0x0040)
#define CTRL_D       (0x0080)
#define CTRL_HEALTH  (0x0100)
#define CTRL_METER   (0x0200)
#define CTRL_PAUSE   (0x0400)

#define STATE_POS_MASK (0x3)
#define STATE_POS_DUCK (0x1)
#define STATE_POS_AIR  (0x2)

#define STATE_STUN_MASK    (0xC)
#define STATE_STUN_ATTACK  (0x4)
#define STATE_STUN_GBOUNCE (0x4)
#define STATE_STUN_HIT     (0x8)
#define STATE_STUN_BLOCK   (0xC)

#define STATE_HITTING_MASK (0x10)
#define STATE_HITTING (0x10)

/* this volume stuff comes from MTSP */
#define BGM_VOLUME_BREAK (0x0040347EUL)
#define SE_VOLUME_BREAK (0x0040348CUL)

#define VOLUME_SET_1_CADDR ((void*)0x00403401UL)
static BYTE VOLUME_SET_1_CODE[] = {0x58,0x8B,0x08,0x6A,0x01,0xEB,0x0D,0x58,
                                   0x8B,0x08,0x6A,0x00,0xEB,0x06,0x90,0xE9,
                                   0xEB,0x21,0x01,0x00,0x6A,0x00,0x6A,0x00,
                                   0x50,0xFF,0x51,0x30,0x5B,0xC3};
#define VOLUME_SET_2_CADDR ((void*)0x0040347BUL)
static BYTE VOLUME_SET_2_CODE[] = {0x50,0x8B,0x08,0xCC,0x52,0x50,0xFF,0x51,
                                   0x3C,0xE9,0x78,0xFF,0xFF,0xFF,0x50,0x8B,
                                   0x08,0xCC,0x52,0x50,0xFF,0x51,0x3C,0xE9,
                                   0x71,0xFF,0xFF,0xFF};

#if 0
/* old lunaport-based stuff */
#define RANDOM_SEED_VADDR ((void*)0x0041fb1cUL)

#define STAGE_SELECT_FADDR ((void*)0x00408756UL)
#define STAGE_SELECT_BREAK (0x00408756UL)
static unsigned char stage_select_func[] = {0xCC,0x90,0x90,0x90,0x90};

#define CONTROL_CHANGE_BREAK (0x0041B588UL)
#define CONTROL_CHANGE_FADDR ((void*)0x0041B588UL)

#define P1_KBD_CONTROLS_VADDR ((void*)0x00425980UL)
#define P2_KBD_CONTROLS_VADDR ((void*)0x00425991UL)
#define KBD_CONTROLS_SIZE (10U)
static unsigned char kbd_control_buffer[KBD_CONTROLS_SIZE];

#define P1_JOY_CONTROLS_VADDR ((void*)0x00445710UL)
#define P2_JOY_CONTROLS_VADDR ((void*)0x00445717UL)
#define JOY_CONTROLS_SIZE (7U)
static unsigned char joy_control_buffer[JOY_CONTROLS_SIZE];

#define KBD_WRITEBACK_FADDR ((void*)0x00414FCAUL)
static unsigned char kbd_writeback_func[] = {
	0x90,0x8B,0x15,0x7C,0x1F,0x54,0x00,0x90,
	0x90,0x90,0x90,0x90
};

#define JOY_WRITEBACK_FADDR ((void*)0x0041502CUL)
static unsigned char joy_writeback_func[] = {
	0x90,0x8B,0x15,0x7C,0x1F,0x54,0x00,0x90,
	0x90,0x90,0x90,0x90
};

#define STICK_SELECTION_FADDR ((void*)0x004144CBUL)
static unsigned char stick_selection_func[] = {0x6A,0x00};

#define LOCAL_INPUT_FADDR ((void*)0x0041B588UL)
#define LOCAL_INPUT_BREAK (0x0041B591UL)
static unsigned char local_input_func[] = {
	0xCC,0x90,0x90,0x90,0xE8,0xAF,0x8D,0xFF,
	0xFF,0xCC,0x90,0x90,0x90,0x90,0x90,0x90,
	0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,
	0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,
	0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,
	0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,
	0x90
};
#define LOCAL_INPUT_JMPBACK ((void*)0x0041B5B9UL)
static unsigned char local_input_jmpback[2][5] = {
	{
		0xE9,0x82,0x91,0xFF,0xFF
	},
	{
		0xE9,0x9D,0x91,0xFF,0xFF
	},
};

#define REMOTE_INPUT_FADDR ((void*)0x0041B5BEUL)
#define REMOTE_INPUT_BREAK (0x0041B5C0UL)
static unsigned remote_input_func[] = {
	0x90,0x90,0xCC,0x90,0x90,0x90,0x90,0x90,
	0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,
	0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,
	0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,
	0x90,0x90,0x90,0x90,0x90
};
#define REMOTE_INPUT_JMPBACK ((void*)0x0041B5E3UL)
static unsigned char remote_input_jmpback[2][5] = {
	{
		0xE9,0x58,0x91,0xFF,0xFF
	},
	{
		0xE9,0x73,0x91,0xFF,0xFF
	},
};

#define P1_JUMP_FADDR ((void*)0x0041473bUL)
static unsigned char p1_jump_func[2][5] = {
	{
		0xE9,0x48,0x6E,0x00,0x00
	},
	{
		0xE9,0x7E,0x6E,0x00,0x00
	},
};

#define P2_JUMP_FADDR ((void*)0x00414756UL)
static unsigned char p2_jump_func[2][5] = {
	{
		0xE9,0x2D,0x6E,0x00,0x00
	},
	{
		0xE9,0x63,0x6E,0x00,0x00
	},
};

#define REPLAY_HOOKS_FADDR ((void*)0x0041B760UL)
static unsigned char replay_hooks_func[] = {
	0xE8,0xDB,0x8B,0xFF,0xFF,0xCC,0xE9,0xD5,
	0x8F,0xFF,0xFF,0xE8,0xD0,0x8B,0xFF,0xFF,
	0xCC,0xE9,0xE5,0x8F,0xFF,0xFF
};
#define REPLAY_P1_BREAK (0x0041B765UL)
#define REPLAY_P2_BREAK (0x0041B770UL)

#define REPLAY_P1_JUMP_FADDR ((void*)0x0041473BUL)
static unsigned char replay_p1_jump_func[] = {0xE9,0x20,0x70,0x00,0x00};

#define REPLAY_P2_JUMP_FADDR ((void*)0x00414756UL)
static unsigned char replay_p2_jump_func[] = {0xE9,0x10,0x70,0x00,0x00};

#define SINGLEP_HOOKS_FADDR ((void*)0x0041B776UL)
#define SINGLEP_CONTROL_BREAK (0x0041B776UL)
#define SINGLEP_INPUT_BREAK (0x0041B77CUL)
static unsigned char singlep_hooks_func[] = {
	0xCC,0xE8,0xC4,0x8B,0xFF,0xFF,0xCC,0xE9,
	0x9E,0x8F,0xFF,0xFF
};
#define SINGLEP_JUMP_FADDR ((void*)0x0041471BUL)
static unsigned char singlep_jump_func[] = {0xE9,0x56,0x70,0x00,0x00};

#define FRAME_FADDR ((void*)0x0041B782UL)
#define FRAME_BREAK (0x0041B782UL)
static unsigned char frame_func[] = {
	0xCC,0xA1,0x6C,0x47,0x42,0x00,0xE9,0x88,
	0x94,0xFE,0xFF
};

#define FRAME_JUMP ((void*)0x00404C10UL)
static unsigned char frame_jump_func[] = {0xE9,0x6D,0x6B,0x01,0x00};

#define TITLE_FADDR ((void*)0x00403F4EUL)
#define TITLE_BREAK (0x00403F4EUL)
static unsigned char title_break_func[] = {0xCC};
static unsigned char title_break_func_bak[] = {0x52};

#define GAME_SPEED_VADDR ((void*)0x0041E2F0UL)
#define KEYSTATES_VADDR ((void*)0x00424D20UL)
#define WAITSKIP 100
#define SPEED_MOD 3

#define EXTRA_INPUT_JUMP_FADDR ((void*)0x00405B45UL)
static unsigned char extra_input_jump_func[] = {0xE9,0x56,0x5C,0x01,0x00,0x90};

#define IMPORT_KBD_FADDR ((void*)0x004146D9UL)
#define EXPORT_KBD_FADDR ((void*)0x0041B7B1UL)
static unsigned char getkeyboardstate_func[6];

#define EXTRA_INPUT_FADDR ((void*)0x0041B7A0UL)
#define EXTRA_INPUT_WAIT_BREAK (0x0041B7A6UL)
#define EXTRA_INPUT_BREAK (0x0041B7B7UL)
#define EXTRA_INPUT_GO ((void*)0x0041B7ACUL)
#define EXTRA_INPUT_BACK ((void*)0x00405B4AUL)
static unsigned char extra_input_func[] = {
	0x8B,0x35,0xF0,0xE2,0x41,0x00,0xCC,0xE9,
	0x9E,0xA3,0xFE,0xFF,0x68,0x20,0x4D,0x42,
	0x00,0x90,0x90,0x90,0x90,0x90,0x90,0xCC
};

#define P1_CHAR_NUM_VADDR ((void*)0x00470020UL)
#define P2_CHAR_NUM_VADDR ((void*)0x00470024UL)
#define STAGE_NUM_VADDR ((void*)0x00470040UL)
#define CHAR_NAMES_VADDR ((void*)0x00435474UL)
#define STAGE_NAMES_VADDR ((void*)0x0043A29CUL)

#define LOAD_STAGE_BREAK (0x004041E0UL)
#define DOUBLE_KO_BREAK (0x00408FCFUL)
#define DRAW_BREAK (0x00408FF7UL)
#define P1_WIN_BREAK (0x0040901FUL)
#define P2_WIN_BREAK (0x00409043UL)
unsigned int simple_int3[] = {
	LOAD_STAGE_BREAK,
	DOUBLE_KO_BREAK,
	DRAW_BREAK,
	P1_WIN_BREAK,
	P2_WIN_BREAK,
	0
};
unsigned char simple_int3_bak[] = {
	0,
	0,
	0,
	0,
	0,
	0
};

#define FPS_HACK_FADDR ((void*)0x00405B45UL)
#define MAX_INPUTS_PER_FRAME_VADDR ((void*)0x00405BC7UL)
#endif

#endif /* CTU_INJECT_H */
