OBJS = ctu.o res.o menu.o info.o
CC = i586-mingw32msvc-gcc
LD = i586-mingw32msvc-gcc
CFLAGS = -Wall -O2
LDLIBS = -lwinmm -lcomdlg32 -lshlwapi
LDFLAGS = -mwindows

EXE = ctu.exe

$(EXE): $(OBJS)
	$(LD) $(OBJS) -o $@ $(LDLIBS) $(LDFLAGS)

res.rc: res.rc.m4
	m4 res.rc.m4 > $@.tmp
	mv -f $@.tmp $@

res.o: res.rc
	i586-mingw32msvc-windres res.rc -o $@

clean:
	rm -f *.o $(EXE)
