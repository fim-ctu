#include "ctu.h"

#include <windows.h>
#include <stdio.h>

#include "res.h"
#include "inject.h"
#include "menu.h"

#define DLGITEM(x) { #x, CTU_DLGITEM_ ## x }

static struct dlg_item dlg_action[] = {
	DLGITEM(None),
	DLGITEM(Recover),
	DLGITEM(Control),
	DLGITEM(Record),
	DLGITEM(Replay),
	{ NULL },
};

static struct dlg_item dlg_health[] = {
	DLGITEM(Normal),
	DLGITEM(Refill),
	{ NULL },
};

static struct dlg_item dlg_meter[] = {
	DLGITEM(Normal),
	DLGITEM(0_Bars),
	DLGITEM(1_Bar),
	DLGITEM(2_Bars),
	DLGITEM(3_Bars),
	{ NULL },
};

static struct dlg_item dlg_magic[] = {
	DLGITEM(Normal),
	DLGITEM(Refill),
	DLGITEM(Deplete),
	{ NULL },
};

static struct dlg_item dlg_info[] = {
	DLGITEM(Off),
	DLGITEM(On),
	{ NULL },
};

#undef DLGITEM

struct ctu_dlg dlg[] = {
	{ "Action:", &dlg_action[0], 1, },
	{ "Health:", &dlg_health[0], 1, },
	{ "Meter:", &dlg_meter[0], 0, },
	{ "Magic:", &dlg_magic[0], 0, },
	{ "Info:", &dlg_info[0], 1, },
};
int n_dlg = sizeof(dlg) / sizeof(dlg[0]);

static int dlg_cur = 0;

static HWND hwndMenu;

static int fm2k_win_set;
static HWND fm2k_win;

static BOOL CALLBACK
find_fm2k_win(HWND hwnd, LPARAM lParam)
{
	int code;
	static char buf[16];

	code = GetClassName(hwnd, buf, sizeof(buf));
	if (code == 0) {
		ctud("GetClassName failed with %d\n", (int)GetLastError());
		return 1;
	}

	if (!strcasecmp(buf, "KGT2KGAME")) {
		if (fm2k_win_set) {
			ctud("Found multiple FM2K windows? old: 0x%lx new: 0x%lx\n",
			       (unsigned long)fm2k_win, (unsigned long)hwnd);
			return 1;
		}
		fm2k_win_set = 1;
		fm2k_win = hwnd;
	}

	return 1;
}

static int
get_fm2k_win(void)
{
	if (!fm2k_win_set) {
		if (!EnumThreadWindows(proc_thread_id, &find_fm2k_win, 0)) {
			ctud("EnumThreadWindows failed with %d\n", (int)GetLastError());
		}
	}

	if (!fm2k_win_set) {
		return 0;
	}

	return 1;
}

static void
handle_title(void)
{
	if (get_fm2k_win()) {
		if (!SetWindowText(fm2k_win, "FightingIsMagic (Collective Training Unit)")) {
			ctud("SetWindowText failed with %d\n", (int)GetLastError());
		}
	}
}

int
get_menu_coords(RECT *rc_win, RECT *rc_client)
{
	WINDOWINFO winfo;

	if (!get_fm2k_win()) {
		return 0;
	}

	ZeroMemory(&winfo, sizeof(winfo));
	winfo.cbSize = sizeof(winfo);

	if (!GetWindowInfo(fm2k_win, &winfo)) {
		ctud("GetWindowInfo failed with %d\n", (int)GetLastError());
		return 0;
	}

	if (rc_win) {
		*rc_win = winfo.rcWindow;
	}
	if (rc_client) {
		*rc_client = winfo.rcClient;
	}

	return 1;
}

static void
setctrl(HWND dlgHwnd, int id, const char *str)
{
	char buf[64];
	char *cur;
	HWND hwnd;

	strcpy(buf, str);
	for (cur = buf; *cur; cur++) {
		if (*cur == '_') {
			*cur = ' ';
		}
	}

	hwnd = GetDlgItem(dlgHwnd, id);
	if (!hwnd) {
		ctud("Error %d from GetDlgItem\n", (int)GetLastError());
		return;
	}

	if (!SetWindowText(hwnd, buf)) {
		ctud("Error %d from SetWindowText\n", (int)GetLastError());
	}
}

static void
setlabel(HWND dlgHwnd, int index, const char *str)
{
	setctrl(dlgHwnd, CTU_CTRL_LBL + index, str);
}

static void
setval(HWND dlgHwnd, int index, const char *str)
{
	setctrl(dlgHwnd, CTU_CTRL_CENT + index, str);
}

static void
setactive(HWND dlgHwnd, int index)
{
	if (index != dlg_cur) {
		setctrl(dlgHwnd, CTU_CTRL_LEFT + dlg_cur, "");
		setctrl(dlgHwnd, CTU_CTRL_RIGHT + dlg_cur, "");
	}
	setctrl(dlgHwnd, CTU_CTRL_LEFT + index, "<");
	setctrl(dlgHwnd, CTU_CTRL_RIGHT + index, ">");
}

static BOOL CALLBACK
menu_dlg_proc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_INITDIALOG: {
		RECT rc;
		int i;

		for (i = 0; i < n_dlg; i++) {
			struct ctu_dlg *cur = &dlg[i];

			setlabel(hwnd, i, cur->label);
			setval(hwnd, i, cur->val_arr[cur->cur].name);

			if (i == dlg_cur) {
				setactive(hwnd, i);
			}
		}

		if (get_menu_coords(NULL, &rc)) {
			int center, width;

			center = (rc.left + rc.right) / 2;
			width = 300;

			rc.left = center - width/2;
			/* actually the width */
			rc.right = width;
			/* actually the height */
			rc.bottom = 200;
			rc.top = rc.top + 100;

			ctud("Setting window pos\n");
			if (!SetWindowPos(hwnd,
			                  HWND_TOPMOST,
			                  rc.left, rc.top,
			                  rc.right,
			                  rc.bottom,
			                  SWP_SHOWWINDOW | SWP_NOACTIVATE)) {
				ctud("SetWindowPos error %d\n", (int)GetLastError());
			}
		}
		break;
	}

	}
	return FALSE;
}

static void
handle_pauseinput(int val)
{
	int delta_h = 0, delta_v = 0;

	/*ctud("pause menu got input 0x%x\n", (unsigned)val);*/

	if ((val & CTRL_BACK)) {
		delta_h--;
	}
	if ((val & CTRL_FWD)) {
		delta_h++;
	}
	if ((val & CTRL_DOWN)) {
		delta_v++;
	}
	if ((val & CTRL_UP)) {
		delta_v--;
	}

	if (delta_v) {
		int newcur = dlg_cur;
		newcur += delta_v;
		if (newcur < 0) {
			newcur = n_dlg - 1;
		}
		if (newcur >= n_dlg) {
			newcur = newcur % n_dlg;
		}
		setactive(hwndMenu, newcur);
		dlg_cur = newcur;
	}

	if (delta_h) {
		struct ctu_dlg *curdlg = &dlg[dlg_cur];
		int cur = curdlg->cur;

		cur += delta_h;

		if (cur < 0) {
			for (cur = 0; curdlg->val_arr[cur].name; cur++)
				;
			cur--;
		}

		if (!curdlg->val_arr[cur].name) {
			cur = 0;
		}

		setval(hwndMenu, dlg_cur, curdlg->val_arr[cur].name);
		curdlg->cur = cur;
	}
}

int
menu_loop(void)
{
	static HANDLE hwndInfo;
	MSG msg;
	DWORD code;

	while ((code = GetMessage(&msg, NULL, 0, 0)) != 0) {
		if (code == -1) {
			ctud("GetMessage got error: %d\n", (int)GetLastError());
			return 1;
		}
		switch (msg.message) {
		case CTU_MENUOFF:
			if (IsWindow(hwndMenu)) {
				DestroyWindow(hwndMenu);
				hwndMenu = NULL;
			} else {
				ctud("menu not on?\n");
			}
			break;
		case CTU_MENUON:
			ctud("got MENUON message\n");
			if (IsWindow(hwndMenu)) {
				ctud("menu already on?\n");
			} else {
				hwndMenu = CreateDialog(hinst,
				                        MAKEINTRESOURCE(CTU_MENUDLG),
				                        NULL,
				                        menu_dlg_proc);

				ShowWindow(hwndMenu, SW_SHOWNOACTIVATE);
			}
			break;

		case CTU_MENUINPUT:
			handle_pauseinput((int)msg.wParam);
			break;

		case CTU_SETTITLE:
			handle_title();
			break;

		case CTU_INFOOFF:
			if (IsWindow(hwndInfo)) {
				DestroyWindow(hwndInfo);
				hwndInfo = NULL;
			} else {
				ctud("info dlg not on?\n");
			}
			break;

		case CTU_INFOON:
			if (IsWindow(hwndInfo)) {
				ctud("info dlg already on?\n");
			} else {
				hwndInfo = CreateDialog(hinst, MAKEINTRESOURCE(CTU_INFODLG), NULL,
				                        info_dlg_proc);
				ShowWindow(hwndInfo, SW_SHOWNOACTIVATE);
			}
			break;

		case CTU_INFOUPDATE:
			info_update(hwndInfo);
			break;

		case CTU_INPUT:
			if (IsWindow(hwndInfo)) {
				info_input(hwndInfo, (int)msg.lParam);
			}
			break;

		case CTU_THREADUPDATE:
			if (!AttachThreadInput(mainthread, proc_thread_id, TRUE)) {
				ctud("AttachThreadInput error %d\n", (int)GetLastError());
			}
			break;

		case WM_QUIT:
			return 0;

		default:
			if (IsWindow(hwndMenu) && IsDialogMessage(hwndMenu, &msg)) {
				/* noop; IsDialogMessage dispatches the message for us */
			} else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}

	return 0;
}
