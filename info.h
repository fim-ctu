#ifndef CTU_INFO_H
#define CTU_INFO_H

struct info_val {
	int res;
	int val[2];
	float fval;
	int isfloat;
	int isplus;
	int dirty;
};

extern BOOL CALLBACK info_dlg_proc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
extern void info_update(HWND hwnd);
extern void info_input(HWND hwnd, int ctrl);

extern struct info_val info_p1_health;
extern struct info_val info_p2_health;

extern struct info_val info_p1_meter;
extern struct info_val info_p2_meter;

extern struct info_val info_p1_magic;
extern struct info_val info_p2_magic;

extern struct info_val info_curc_dmg;
extern struct info_val info_maxc_dmg;

extern struct info_val info_curc_meter;
extern struct info_val info_maxc_meter;

extern struct info_val info_curc_magic;
extern struct info_val info_maxc_magic;

#endif /* CTU_INFO_H */
